public class Cadastro
{
    private int creditossemanais, valorporhora, horasmensais;
    private String formacao, nome, disciplinas;
    private double salario;
    private boolean ead;
    public Cadastro()
    {
        creditossemanais=0;
        formacao=nome=disciplinas="";
        ead=false;
        horasmensais=0;
        salario = 0;
    }
    
    
    public String Professor()
    {
        return nome;
    }
    public String Disciplinas ()
    {
        return disciplinas;
    }
    public double salario(String formacao, boolean ead, int creditossemanais)
    {
        horasmensais = 4*creditossemanais;
        valorporhora=25;
        salario=(horasmensais*valorporhora);
    if (formacao=="graduado" && ead==false)
    {
        return salario;
    }
    else if (formacao=="especializado" && ead==false)
    {
        return salario*1.15;
    }
    else if (formacao=="mestre" && ead==false)
    {
        return salario*1.45;
    }
    else if (formacao=="doutor" && ead==false)
    {
        return salario*1.75;
    }
    else if (formacao=="graduado" && ead==true)
    {
        return salario*0.75;
    }
    else if (formacao=="especializado" && ead==true)
    {
        return (salario*1.15)*0.75;
    }
    else if (formacao=="mestre" && ead==true)
    {
        return (salario*1.45)*0.75;
    }
    else if (formacao=="doutor" && ead==true)
    {
        return (salario*1.75)*0.75;
    }
    else
    {
        return 0;
    }
    }
    
    public void setnome (String nome)
    {
        this.nome=nome;
    }
    public String getnome()
    {
        return nome;
    }
    public void setdisciplinas(String disciplinas)
    {
        this.disciplinas=disciplinas;
    }
    public String getdisciplinas()
    {
        return disciplinas;
    }
    public void setcreditossemanais(int creditos)
    {
        creditos=creditossemanais;
    }
    public int getcreditossemanais()
    {
        return creditossemanais;
    }
}
